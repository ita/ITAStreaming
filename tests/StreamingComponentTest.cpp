// $Id: $

#include <ITAStreamingComponent.h>
#include <ITAStreamingVirtualDevice.h>
#include <ITAStreamingFunctionGenerator.h>
#include <ITAException.h>


#include <iostream>
using namespace std;

void testChannels() {
	ITAStreamingProperties sp(44100, 0, 128);
	ITAStreamingComponent c(sp);
	int id = c.AddInput(42);
	cout << "Input ID = " << id << endl;

	id = c.AddInput(2);
	cout << "Input ID = " << id << endl;

	/* Error: Channels < 1
	id = c.AddInput(0);
	cout << "Input ID = " << id << endl;
	*/

	// Remove an input which does not exist
	//c.RemoveInput(42);

	c.RemoveInput(0);
}

void testConnections() {
	ITAStreamingProperties sp(44100, 0, 128);
	ITAStreamingComponent a(sp);
	a.AddOutput(2);
	ITAStreamingComponent b(sp);
	b.AddInput(2);
	a.GetOutput(0).Connect(b.GetInput(0));
}

// Test component with one input and one output
class MyAmp : public ITAStreamingComponent {
public:
	MyAmp(const ITAStreamingProperties& oProps)
	: ITAStreamingComponent(oProps)
	{
		AddInput(oProps.iNumChannels);
		AddOutput(oProps.iNumChannels);
	}

	void ProcessStream(const InputList& inputs,
		               const OutputList& outputs,
					   const ITAStreamingState& state)
	{
		const ITASampleFrame& sfInput = *inputs[0].pBuf;
		ITASampleFrame& sfOutput = *outputs[0].pBuf;

		// Copy
		sfOutput.write(sfInput, GetStreamProperties().iBlockLength);
		//sfOutput = sfInput;
	}
};
//
void testConnections2()
{
	ITAStreamingProperties sp(44100, 1, 128);
	ITAStreamingState st;
	
	ITAStreamingVirtualDevice dev(44100, 0, 1, 128);
	ITAStreamingFunctionGenerator fg(sp, ITAStreamingFunctionGenerator::SINE, 440, 1, true);
	MyAmp amp(sp);

	Connect(&fg, &amp);
	Connect(&amp, &dev);
	//amp.GetInput(0).Connect( fg.GetOutput(0) );
	//dev.GetInput(0).Connect( amp.GetOutput(0) );
	//amp.GetInput(0).Connect( fg.GetOutput(0) );

	dev.Trigger();

}

int main(int argc, char* argv[])
{
	try
	{
		//testChannels();
		testConnections2();
		return 0;
	}
	catch (ITAException& e)
	{
		cerr << e << endl;
		return 255;
	}
}
