#include <ITAStreamingProperties.h>

ITAStreamingProperties::ITAStreamingProperties()
	: dSampleRate( 0 )
	, iNumChannels( 0 )
	, iBlockLength( 0 )
{
}

ITAStreamingProperties::ITAStreamingProperties( double dSampleRate_, int iNumChannels_, int iBlockLength_ )
	: dSampleRate( dSampleRate_ )
	, iNumChannels( iNumChannels_ )
	, iBlockLength( iBlockLength_ )
{
}

ITAStreamingProperties::~ITAStreamingProperties()
{
}

bool ITAStreamingProperties::operator==( const ITAStreamingProperties& rhs ) const
{
	return ( ( dSampleRate == rhs.dSampleRate ) && ( iNumChannels == rhs.iNumChannels ) && ( iBlockLength == rhs.iBlockLength ) );
}

bool ITAStreamingProperties::operator!=( const ITAStreamingProperties& rhs ) const
{
	return ( ( dSampleRate != rhs.dSampleRate ) && ( iNumChannels != rhs.iNumChannels ) && ( iBlockLength != rhs.iBlockLength ) );
}

bool ITAStreamingProperties::IsValid() const
{
	return ( ( dSampleRate > 0 ) && ( iNumChannels > 0 ) && ( iBlockLength > 0 ) );
}
