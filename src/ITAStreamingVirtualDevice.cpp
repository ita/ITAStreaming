// $Id: $

#include <ITAStreamingVirtualDevice.h>
#include <assert.h>

ITAStreamingVirtualDevice::ITAStreamingVirtualDevice(double dSamplerate, int iInputChannels, int iOutputChannels, int iBlocklength)
: ITAStreamingComponent( ITAStreamingProperties(dSamplerate, 0, iBlocklength), TRIGGER_COMPONENT )
{
	assert( (iInputChannels>=0) && (iOutputChannels>0) ); // Mandatory: output channels >= 1

	// Device inputs = component outputs and vice versa
	AddInput( iOutputChannels );
	if (iInputChannels>0) AddOutput( iInputChannels );

	Reset();
}

ITAStreamingVirtualDevice::~ITAStreamingVirtualDevice()
{
}

void ITAStreamingVirtualDevice::Reset() {
	m_oStreamState.nSamples = 0;
	m_oStreamState.dTimeCode = 0;
}

void ITAStreamingVirtualDevice::Trigger()
{
	//ITAStreamComponent::TriggerStream(m_oStreamState);
	m_oStreamState.nSamples += GetStreamProperties().iBlockLength;
	m_oStreamState.dTimeCode += static_cast<double>( m_oStreamState.nSamples ) / GetStreamProperties().dSampleRate;
}
