#include <ITAStreamingComponent.h>

#include <ITAStreamingSource.h>
#include <ITAException.h>
#include <algorithm>
#include <cassert>

class ITAStreamingComponentOutputImpl : public ITAStreamingComponentOutput
{
public:
	int m_iID;
	ITAStreamingComponent* m_pParent;
	ITAStreamingProperties m_oStreamProps;
	ITASampleFrame m_oOutputBuffer;

	ITAStreamingComponentOutputImpl(int iID, ITAStreamingComponent* pParent, const ITAStreamingProperties& oProps)
	: m_iID(iID),
	  m_pParent(pParent),
	  m_oStreamProps(oProps),
	  m_oOutputBuffer( oProps.iNumChannels, oProps.iBlockLength, false)
	{}

	int GetID() const {
		return m_iID;
	}

	ITAStreamingComponent& GetStreamComponent() const {
		return *m_pParent;
	}

	const ITAStreamingProperties& GetStreamProperties() const {
		return m_oStreamProps;
	}

	void Connect(ITAStreamingComponentInput& pDest) {
		// Realize from the input-side
		pDest.Connect(*this);
	}

	friend class ITAStreamingComponent;
};

class ITAStreamingComponentInputImpl : public ITAStreamingComponentInput {
public:
	int m_iID;
	ITAStreamingComponent* m_pParent;
	ITAStreamingProperties m_oStreamProps;
	ITAStreamingComponentOutputImpl* m_pSource;
	const ITASampleFrame* m_pSourceBuffer;

	ITAStreamingComponentInputImpl(int iID, ITAStreamingComponent* pParent, const ITAStreamingProperties& oProps)
	: m_iID(iID),
	  m_pParent(pParent),
	  m_oStreamProps(oProps),
	  m_pSource(nullptr),
	  m_pSourceBuffer(&pParent->m_sfSilence)
	{}

	int GetID() const {
		return m_iID;
	}

	ITAStreamingComponent& GetStreamComponent() const {
		return *m_pParent;
	}

	const ITAStreamingProperties& GetStreamProperties() const {
		return m_oStreamProps;
	}


	bool IsConnected() const {
		return (m_pSource != nullptr);
	}

	void Connect(ITAStreamingComponentOutput& oSource) {
		// Realize within the parent component
		m_pParent->ConnectInput(this, dynamic_cast<ITAStreamingComponentOutputImpl*>( &oSource ));
	}

	void Disconnect() {
		// Realize within the parent component
		m_pParent->DisconnectInput(this);
	}

	friend class ITAStreamingComponent;
};

ITAStreamingComponent::ITAStreamingComponent(const ITAStreamingProperties& oProps, ITAStreamComponentMode eMode)
: m_oStreamProps(oProps),
  m_eMode(eMode),
  m_sfSilence( 1, oProps.iBlockLength, true),
  m_iInputIDCounter(0), m_iOutputIDCounter(0)
{
	// Note: iChannels is ignored.
	assert( (m_oStreamProps.dSampleRate > 0) && (m_oStreamProps.iBlockLength > 0 ) );

	Reset();
}

const ITAStreamingProperties& ITAStreamingComponent::GetStreamProperties() const {
	return m_oStreamProps;
}

void ITAStreamingComponent::Reset() {
	m_oStreamState.nSamples = ITAStreamingState::UNDEFINED;	// Used for comparisons
}	

int ITAStreamingComponent::GetNumInputs() const {
	return static_cast<int>( m_pmInputs.size() );
}

ITAStreamingComponentInput& ITAStreamingComponent::GetInput(int iID) const {
	InputMap::const_iterator cit = m_pmInputs.find(iID);
	if (cit == m_pmInputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid input ID");
	return *(cit->second);
}

int ITAStreamingComponent::AddInput(int nChannels) {
	if (nChannels < 1)
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid number of channels");

	int iID( m_iInputIDCounter++ );
	ITAStreamingProperties oStreamProps( m_oStreamProps );
	oStreamProps.iNumChannels = nChannels;
	ITAStreamingComponentInputImpl* pInput( new ITAStreamingComponentInputImpl(iID, this, oStreamProps) );
	m_pmInputs.insert( std::pair<int, ITAStreamingComponentInputImpl*>(iID, pInput) );

	// Add the buffer to the list
	InputDesc desc;
	desc.iID = iID;
	desc.pBuf = pInput->m_pSourceBuffer; // TODO: Doof so... Muss anders gebaut werden.
	m_vInputList.push_back(desc);

	return iID;
}

void ITAStreamingComponent::RemoveInput(int iID) {
	InputMap::iterator it = m_pmInputs.find(iID);
	if (it == m_pmInputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid input ID");
	m_pmInputs.erase(it);
}

int ITAStreamingComponent::GetNumOutputs() const {
	return static_cast<int>( m_pmOutputs.size() );
}

ITAStreamingComponentOutput& ITAStreamingComponent::GetOutput(int iID) const {
	OutputMap::const_iterator cit = m_pmOutputs.find(iID);
	if (cit == m_pmOutputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid output ID");
	return *(cit->second);
}

int ITAStreamingComponent::AddOutput(int nChannels) {
	if (nChannels < 1)
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid number of channels");

	int iID( m_iOutputIDCounter++ );
	ITAStreamingProperties oStreamProps( m_oStreamProps );
	oStreamProps.iNumChannels = nChannels;
	ITAStreamingComponentOutputImpl* pOutput = new ITAStreamingComponentOutputImpl(iID, this, oStreamProps);
	m_pmOutputs.insert( std::pair<int, ITAStreamingComponentOutputImpl*>(iID, pOutput) );

	// Add the buffer to the list
	OutputDesc desc;
	desc.iID = iID;
	desc.pBuf = &pOutput->m_oOutputBuffer; // TODO: Doof so... Muss anders gebaut werden.
	m_vOutputList.push_back(desc);
	return iID;
}

void ITAStreamingComponent::RemoveOutput(int iID) {
	OutputMap::iterator it = m_pmOutputs.find(iID);
	if (it == m_pmOutputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid output ID");
	m_pmOutputs.erase(it);
}

const ITASampleFrame& ITAStreamingComponent::GetStreamInputBuffer(int iInputID) {
	InputMap::const_iterator cit = m_pmInputs.find(iInputID);
	if (cit == m_pmInputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid input ID");
	
	ITAStreamingComponentInputImpl* pInput = dynamic_cast<ITAStreamingComponentInputImpl*>( cit->second );
	return *pInput->m_pSourceBuffer;
}

ITASampleFrame& ITAStreamingComponent::GetStreamOutputBuffer(int iOutputID) {
	OutputMap::const_iterator cit = m_pmOutputs.find(iOutputID);
	if (cit == m_pmOutputs.end())
		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid output ID");
	
	ITAStreamingComponentOutputImpl* pOutput = dynamic_cast<ITAStreamingComponentOutputImpl*>( cit->second );
	return pOutput->m_oOutputBuffer;
}

void ITAStreamingComponent::ProcessStream(const ITAStreamingComponent::InputList& vInputs,
	                                   const ITAStreamingComponent::OutputList& vOutputs,
									   const ITAStreamingState& oStreamState) {
	// The base class implement no DSP at all ...
	// Do this yourself by reimplementing this hook!
}

void ITAStreamingComponent::PullInputs(const ITAStreamingState& oStreamState) {
	for (ComponentTable::iterator it=m_tInputComponents.begin(); it!=m_tInputComponents.end(); ++it)
		 it->first->PullOutputs(oStreamState);
}

void ITAStreamingComponent::PullOutputs(const ITAStreamingState& oStreamState) {
	// Update check
	assert( m_oStreamState != oStreamState );  // Strict mode
	if (m_oStreamState == oStreamState) return;

	assert( (m_oStreamState.nSamples < oStreamState.nSamples) || // Monotony
		    (m_oStreamState.nSamples == ITAStreamingState::UNDEFINED) ); 
	m_oStreamState = oStreamState;

	// TODO: Check order
	PullInputs(m_oStreamState);
	ProcessStream(m_vInputList, m_vOutputList, oStreamState);
}

void ITAStreamingComponent::ConnectInput(ITAStreamingComponentInputImpl* pInput, ITAStreamingComponentOutputImpl* pOutput) {
	// Disallow connection from a component's inputs and outputs
	assert( pInput && pOutput );
	if (pInput->m_pParent == pOutput->m_pParent)
		ITA_EXCEPT1(INVALID_PARAMETER, "No connections of inputs and outputs of the same component");

	// Ensure matching properties
	if (pInput->GetStreamProperties() != pOutput->GetStreamProperties())
		ITA_EXCEPT1(INVALID_PARAMETER, "Input and output must have equal stream properties");

	// Strict mode: Input must not be connected!
	if (pInput->IsConnected())
		ITA_EXCEPT1(INVALID_PARAMETER, "Input already connected");

	pInput->m_pSource = pOutput;
	pInput->m_pSourceBuffer = &pOutput->m_oOutputBuffer;
	
	ComponentTable::iterator it;
	for (it=m_tInputComponents.begin(); it!=m_tInputComponents.end(); ++it)
		if (it->first == pOutput->m_pParent) break;

	if (it == m_tInputComponents.end())
		// Insert new ref counter
		m_tInputComponents.push_back( std::pair<ITAStreamingComponent*,int>(pOutput->m_pParent, 1) );
	else
		// Increment ref counter
		++(it->second);
}

void ITAStreamingComponent::DisconnectInput(ITAStreamingComponentInputImpl* pInput) {
	if (!pInput->IsConnected()) return;

	ITAStreamingComponentOutputImpl* pOutput( pInput->m_pSource );

	ComponentTable::iterator it;
	for (it=m_tInputComponents.begin(); it!=m_tInputComponents.end(); ++it)
		if (it->first == pOutput->m_pParent) {
			if (--(it->second) == 0) m_tInputComponents.erase(it);
			return;
		}

	assert( false ); // May not be reached!
}


void Connect(ITAStreamingComponent* pSource, ITAStreamingComponent* pDest) {
	if (!pSource)
		ITA_EXCEPT1(INVALID_PARAMETER, "Source component must not be null");

	if (!pDest)
		ITA_EXCEPT1(INVALID_PARAMETER, "Destination component must not be null");

	if (pSource->GetNumOutputs() == 0)
		ITA_EXCEPT1(INVALID_PARAMETER, "Source component has no outputs");
	
	if (pDest->GetNumInputs() == 0)
		ITA_EXCEPT1(INVALID_PARAMETER, "Destination component has no inputs");

	if (pSource->GetNumOutputs() > 1)
		ITA_EXCEPT1(INVALID_PARAMETER, "Connection ambiguous, as the source component has multiple outputs");	

	if (pDest->GetNumInputs() > 1)
		ITA_EXCEPT1(INVALID_PARAMETER, "Connection ambiguous, as the destination component has multiple inputs");

	// TODO: Liste der Ausg�nge. ID muss nicht 0 sein...
	pDest->GetInput(0).Connect( pSource->GetOutput(0) );
}