#include <ITAStreamingBackend.h>
#include "ITAStreamingBackendASIO.h"

ITAStreamingBackend* ITAStreamingBackend::GetDefaultBackend()
{
#ifdef ITA_STREAMING_WITH_BACKEND_ASIO
	return ITAStreamingBackendASIO::GetInstance();
#else
	#ifdef ITA_STREAMING_WITH_BACKEND_PORTAUDIO
		return ITAStreamingBackendPortaudio::GetInstance();
	#else
		return NULL;
	#endif // ITA_STREAMING_WITH_BACKEND_PORTAUDIO
#endif // ITA_STREAMING_WITH_BACKEND_ASIO
}
