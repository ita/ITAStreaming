#include "ITAStreamingBackendASIO.h"

// ITA streaming includes
#include <ITAStreamingState.h>
#include <ITAStreamingProperties.h>

// ITA base includes
#include <ITAException.h>
#include <ITASampleTypeConversion.h>

// STL includes
#include <assert.h>
#include <vector>

#define STREAMING_BACKEND_ASIO 1

#if( STREAMING_BACKEND_ASIO == 0 )

ITAStreamASIOBackend* GetInstance()
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ITAStreaming has been built without ASIO backend support" );
	return NULL;
}

#else // (STREAMING_BACKEND_ASIO == 0)

// Set the ASIO platform (see asiosys.h)
// Transform macro names _WIN32 => WIN32
#if _WIN32
#ifndef WIN32
#define WIN32
#endif
#endif

//#include <asiosys.h>
#include <common/asio.h>
#include <host/asiodrivers.h>
#ifdef WIN32
	#include <host/pc/asiolist.h>
#endif
class ITAStreamBackendASIOImpl : public ITAStreamingBackendASIO
{
public:
	// Singleton allows us to declare all members static
	static AsioDrivers asioDrivers;
	static ASIODriverInfo asioDriverInfo;
	static std::vector<ASIOBufferInfo> asioBufferInfos;
	static std::vector<ASIOChannelInfo> asioChannelInfos;

	static std::vector<ITAStreamingDeviceProperties> vDriverProps;
	static bool bInitialized;
	
	static ITAStreamingState oStreamState;

	ITAStreamBackendASIOImpl()
	{
		// Discover the available drivers
		for( long i=0; i<asioDrivers.asioGetNumDev(); i++ )
		{
			char pszName[MAXDRVNAMELEN];
			assert( asioDrivers.asioGetDriverName(i, pszName, MAXDRVNAMELEN) == 0 );
			vDriverProps.push_back( ITAStreamingDeviceProperties( pszName, 0, 0, 0, 0 ) );
		}
	}

	~ITAStreamBackendASIOImpl()
	{
	}

	// --= Stream backend =--
	
	std::vector<ITAStreamingDeviceProperties> EnumerateDevices()
	{
		return vDriverProps;
	}

	ITAStreamDevice* InitializeDevice(const ITAStreamingDeviceProperties& oProps)
	{
		ASIOError aeResult;

		if (bInitialized)
			ITA_EXCEPT1(MODAL_EXCEPTION, "Driver already initialized");

		// Load the driver, this will set up all the necessary internal data structures	
		if (!asioDrivers.loadDriver(const_cast<char*>( oProps.sName.c_str() )))
			ITA_EXCEPT1(UNKNOWN, "Failed to load ASIO driver \"" + oProps.sName + "\"");

		if ((aeResult = ASIOInit(&asioDriverInfo)) != ASE_OK)
			ITA_EXCEPT1(UNKNOWN, "Failed to initialize ASIO driver \"" + oProps.sName + "\" (ASIO errorcode " + std::to_string( aeResult ) + ")");

		if ((aeResult = ASIOSetSampleRate( oProps.dSampleRate)) != ASE_OK)
			ITA_EXCEPT1(UNKNOWN, "Failed to set sampling rate of " + std::to_string( oProps.dSampleRate ) + " Hz for ASIO driver \"" + 
			oProps.sName + "\" (ASIO errorcode " + std::to_string(aeResult) + ")");

		// Setup buffer infos for inputs and outputs
		int nChannels(oProps.iNumInputChannels + oProps.iNumOutputChannels);
		asioBufferInfos.resize(nChannels);
		asioChannelInfos.resize(nChannels);
		int j(0);

		for (int i = 0; i<oProps.iNumInputChannels; i++) {
			asioBufferInfos[j].buffers[0] = 0;
			asioBufferInfos[j].buffers[1] = 0;
			asioBufferInfos[j].channelNum = i;
			asioBufferInfos[j].isInput = ASIOTrue;  

			asioChannelInfos[j].channel = i;
			asioChannelInfos[j].isInput = ASIOTrue;  

			j++;
		}

		for (int i = 0; i<oProps.iNumOutputChannels; i++) {
			asioBufferInfos[j].buffers[0] = 0;
			asioBufferInfos[j].buffers[1] = 0;
			asioBufferInfos[j].channelNum = i;
			asioBufferInfos[j].isInput = ASIOFalse;  

			asioChannelInfos[j].channel = i;
			asioChannelInfos[j].isInput = ASIOFalse;  

			j++;
		}

		ASIOCallbacks asioCallbacks;
		asioCallbacks.asioMessage = &asioMessage;
		asioCallbacks.bufferSwitch = &bufferSwitch;
		asioCallbacks.bufferSwitchTimeInfo = &bufferSwitchTimeInfo;
		asioCallbacks.sampleRateDidChange = &sampleRateDidChange;

		
		if ((aeResult = ASIOCreateBuffers(&asioBufferInfos[0], static_cast<long>(nChannels), static_cast<long>(oProps.iBlockSize), &asioCallbacks)) != ASE_OK)
			ITA_EXCEPT1(UNKNOWN, "Failed to create buffers for ASIO driver \"" + oProps.sName + "\" (ASIO errorcode " + std::to_string(aeResult) + ")");

		// Check support for the sample types
		for (int i=0; i<nChannels; i++) {
			if ((aeResult = ASIOGetChannelInfo(&asioChannelInfos[i])) != ASE_OK)
				ITA_EXCEPT1(UNKNOWN, "Failed to get channel informations from ASIO driver \"" + oProps.sName + "\" (ASIO errorcode " + std::to_string(aeResult) + ")");

			if ((asioChannelInfos[i].type != ASIOSTInt16LSB) && (asioChannelInfos[i].type != ASIOSTInt32LSB))
				ITA_EXCEPT1(UNKNOWN, "Sample format of ASIO driver \"" + oProps.sName + "\" not supported (ASIO sample type " + std::to_string(asioChannelInfos[i].type) + ")");
		}

		bInitialized = true;

		
	}

	void SetEnabled(bool bEnabled) {
		ASIOError aeResult;

		if (!bInitialized) ITA_EXCEPT1(MODAL_EXCEPTION, "No driver initialized");

		if (bEnabled) {
			// Case: Start the streaming
			if ((aeResult = ASIOStart()) != ASE_OK)
				ITA_EXCEPT1(UNKNOWN, "Failed to start the audio streaming (ASIO errorcode " + std::to_string(aeResult) + ")");
		} else {
			// Case: Stop the streaming
			assert( ASIOStop() == ASE_OK );
		}
	}

	// --= Streaming device =--

	class AudioDevice : public ITAStreamDevice
	{
	public:

		AudioDevice(ITAStreamBackendASIOImpl* pParent, const ITAStreamingDeviceProperties& oProps)
		: ITAStreamDevice( ITAStreamingProperties(oProps.dSampleRate, 0, oProps.iBlockSize) ), m_pParent(pParent), m_oProps(oProps)
		{}

		const ITAStreamingDeviceProperties& GetDeviceProperties() const
		{
			return m_oProps;
		}

		void SetEnabled(bool bEnabled)
		{
			m_pParent->SetEnabled(bEnabled);
		}
	private:
		ITAStreamBackendASIOImpl* m_pParent;
		ITAStreamingDeviceProperties m_oProps;
	};

	// --= ASIO callbacks =--

	/*
	// Singleton allows us to declare all callbacks within the class as static methods
	static long asioMessage(long selector, long value, void* message, double* opt);
	static void bufferSwitch(long doubleBufferIndex, ASIOBool directProcess);
	static ASIOTime* bufferSwitchTimeInfo(ASIOTime* params, long doubleBufferIndex, ASIOBool directProcess);
	static void sampleRateDidChange(ASIOSampleRate sRate);	
	*/

	static long asioMessage(long selector, long value, void* message, double* opt) {
		//printf("[ITAStreamBackendASIOImpl] Received asioMessage\n");
		return 0;
	}

	static void bufferSwitch(long doubleBufferIndex, ASIOBool directProcess) {
		//printf("[ITAStreamBackendASIOImpl] Enter bufferSwitch\n");

		// INFO: This code is based on the ASIO SDK examples

		// As this is a "back door" into the bufferSwitchTimeInfo a timeInfo needs 
		// to be created though it will only set the timeInfo.samplePosition and
		// timeInfo.systemTime fields and the according flags
		ASIOTime timeInfo;
		memset(&timeInfo, 0, sizeof(timeInfo));

		// Get the time stamp of the buffer, not necessary if no
		// synchronization to other media is required
		if (ASIOGetSamplePosition(&timeInfo.timeInfo.samplePosition,&timeInfo.timeInfo.systemTime) == ASE_OK)
			timeInfo.timeInfo.flags = kSystemTimeValid | kSamplePositionValid;

		bufferSwitchTimeInfo(&timeInfo, doubleBufferIndex, directProcess);
	}

	static ASIOTime* bufferSwitchTimeInfo(ASIOTime* params, long doubleBufferIndex, ASIOBool directProcess)
	{
		/*

		int m(0), n(0); // Cursors on inputs and outputs

		for (size_t i=0; i<asioBufferInfos.size(); ++i) {
			if (asioBufferInfos[i].isInput == TRUE) {
				// Case: Input channel
				assert( m < oRecordStreamProps.iChannels );
				float* pfBuf = pRecordStream->GetStreamOutputBuffer()[m].data();

				// TODO: Better types in STC!

				// Convert the samples
				switch (asioChannelInfos[i].type) {
				case ASIOSTInt16LSB:		
					stc_sint16_to_float(pfBuf, static_cast<short*>( asioBufferInfos[i].buffers[doubleBufferIndex] ), oRecordStreamProps.iBlocklength);
					break;

				case ASIOSTInt32LSB:
					stc_sint32_to_float(pfBuf, static_cast<int*>( asioBufferInfos[i].buffers[doubleBufferIndex] ), oRecordStreamProps.iBlocklength);			}

				// TODO: Handle other sample types!

				++m;
			} else {
				// Case: Output channel
				assert( n < oPlaybackStreamProps.iChannels );

				const float* pfBuf = pPlaybackStream->GetStreamData(oStreamState)[n].data();
				assert( pfBuf );

				switch (asioChannelInfos[i].type) {
				case ASIOSTInt16LSB:
					stc_float_to_sint16( static_cast<short*>( asioBufferInfos[i].buffers[doubleBufferIndex] ), pfBuf, oPlaybackStreamProps.iBlocklength );
					break;

				case ASIOSTInt32LSB:
					stc_float_to_sint32( static_cast<int*>( asioBufferInfos[i].buffers[doubleBufferIndex] ), pfBuf, oPlaybackStreamProps.iBlocklength );		      
					break;	

					// TODO: Handle other sample types!

				}

				++n;
			}
		}

		// TODO: Proper stream state implementation
		oStreamState.nSamples += oPlaybackStreamProps.iBlocklength;
		oStreamState.dTimeCode = static_cast<double>( oStreamState.nSamples ) / oPlaybackStreamProps.dSamplerate;
		*/

		return 0;
	}

	static void sampleRateDidChange(ASIOSampleRate sRate) {
		//printf("[ITAStreamBackendASIOImpl] Sampling rate changed\n");
	}
};

AsioDrivers ITAStreamBackendASIOImpl::asioDrivers;
ASIODriverInfo ITAStreamBackendASIOImpl::asioDriverInfo;
std::vector<ASIOBufferInfo> ITAStreamBackendASIOImpl::asioBufferInfos;
std::vector<ASIOChannelInfo> ITAStreamBackendASIOImpl::asioChannelInfos;
std::vector<ITAStreamingDeviceProperties> ITAStreamBackendASIOImpl::vDriverProps;
bool ITAStreamBackendASIOImpl::bInitialized(false);
ITAStreamingState ITAStreamBackendASIOImpl::oStreamState;

ITAStreamingBackendASIO* ITAStreamingBackendASIO::GetInstance()
{
	static ITAStreamBackendASIOImpl oInstance;
	return &oInstance;
}

#endif // (STREAMING_BACKEND_ASIO == 0)