// $Id: ITADatasourceRealization.cpp,v 1.4 2008-09-02 08:53:35 fwefers Exp $

#include <ITAStreamingSourceRealization.h>

#include <assert.h>
#include <limits>

static const long STREAM_POS_INIT( std::numeric_limits< long >::max() );

ITAStreamingSourceRealization::ITAStreamingSourceRealization(const ITAStreamingProperties& oProps)
	: m_oStreamProps( oProps ),
	  m_oOutputBuffer( oProps.iNumChannels, oProps.iBlockLength, false),
	  m_pEventHandler(nullptr)
{
	assert( oProps.IsValid() );
	Reset();
}

ITAStreamingSourceRealization::~ITAStreamingSourceRealization() {}

void ITAStreamingSourceRealization::Reset() {
	m_iStreamPos = STREAM_POS_INIT;	// Used for comparisons
	m_oOutputBuffer.zero();
}	

ITAStreamDatasourceRealizationEventHandler* ITAStreamingSourceRealization::GetStreamEventHandler() const {
	return m_pEventHandler;
}

void ITAStreamingSourceRealization::SetStreamEventHandler(ITAStreamDatasourceRealizationEventHandler* pHandler) {
	m_pEventHandler = pHandler;
}

ITASampleFrame& ITAStreamingSourceRealization::GetStreamOutputBuffer()
{
	return m_oOutputBuffer;
}

const ITASampleFrame& ITAStreamingSourceRealization::GetStreamData(const ITAStreamingState& oState) {
	// Update check
	if (m_iStreamPos != oState.nSamples) {
		assert( (m_iStreamPos < oState.nSamples) || (m_iStreamPos == STREAM_POS_INIT) ); // Monotony

		// Order: Hook first, handler second
		ProcessStreamData(oState);
		if (m_pEventHandler) m_pEventHandler->HandleProcessStreamData(this, oState);

		m_iStreamPos = oState.nSamples;
	}

	return GetStreamOutputBuffer();
}

void ITAStreamDatasourceRealizationEventHandler::HandleProcessStreamData(ITAStreamingSourceRealization*, const ITAStreamingState&) {}
