// $Id:  $
#include <ITAStreamingFunctionGenerator.h>

// ITAStreaming
#include <ITAStreamingSourceRealization.h>

// ITABase
#include <ITAConstants.h>
#include <ITASampleFrame.h>

// STL
#include <algorithm>
#include <assert.h>
#include <math.h>
#include <string>

ITAStreamingFunctionGenerator::ITAStreamingFunctionGenerator(const ITAStreamingProperties& oProperties,
						                               int iSignalFunction,
						                               double dFrequency,
						                               float fAmplitude,
						                               bool bPeriodic)
: ITAStreamingComponent(oProperties),

  // Initial values
  m_iFunction(iSignalFunction),
  m_bPeriodic(bPeriodic),
  m_bMuted(false),
  m_fAmplitude(fAmplitude),
  m_iSampleCount(0),
  m_fPhase(0.0f)
{
	assert( oProperties.IsValid() );
	AddOutput(oProperties.iNumChannels);
	SetFrequency(dFrequency);
	Reset();
}

ITAStreamingFunctionGenerator::~ITAStreamingFunctionGenerator() {}

void ITAStreamingFunctionGenerator::Reset()
{
	m_iSampleCount = 0;
	m_fPhase = 0.0f;
}

int ITAStreamingFunctionGenerator::GetFunction() const
{
	return m_iFunction;
}

void ITAStreamingFunctionGenerator::SetFunction(int iFunction)
{
	m_iFunction = iFunction;
}

double ITAStreamingFunctionGenerator::GetFrequency() const
{
	return GetStreamProperties().dSampleRate / (double) m_iPeriodLengthSamples;
}

void ITAStreamingFunctionGenerator::SetFrequency(double dFrequency)
{
	assert( dFrequency >= 0 );
	m_iPeriodLengthSamples = (int) round( GetStreamProperties().dSampleRate / dFrequency );
}

int ITAStreamingFunctionGenerator::GetPeriodAsSamples() const
{
	return m_iPeriodLengthSamples;
}

void ITAStreamingFunctionGenerator::SetPeriodAsSamples(int iNumSamples)
{
	assert( iNumSamples >= 0 );
	m_iPeriodLengthSamples = iNumSamples;
}

double ITAStreamingFunctionGenerator::GetPeriodAsTime() const
{
	return (double)m_iPeriodLengthSamples / GetStreamProperties().dSampleRate;
}

void ITAStreamingFunctionGenerator::SetPeriodAsTime(double dPeriodLength)
{
	m_iPeriodLengthSamples = (int)round(dPeriodLength * GetStreamProperties().dSampleRate);
}

bool ITAStreamingFunctionGenerator::IsPeriodic() const
{
	return m_bPeriodic;
}

void ITAStreamingFunctionGenerator::SetPeriodic(bool bPeriodic)
{
	m_bPeriodic = bPeriodic;
}

bool ITAStreamingFunctionGenerator::IsMuted() const
{
	return m_bMuted;
}

void ITAStreamingFunctionGenerator::SetMuted(bool bMuted)
{
	m_bMuted = bMuted;
}

float ITAStreamingFunctionGenerator::GetAmplitude() const
{
	return m_fAmplitude;
}

void ITAStreamingFunctionGenerator::SetAmplitude(float fAmplitude) 
{
	m_fAmplitude = (float) fAmplitude;
}

void ITAStreamingFunctionGenerator::ProcessStream(const ITAStreamingComponent::InputList& vInputs,
		                                       const ITAStreamingComponent::OutputList& vOutputs,
					                           const ITAStreamingState& oStreamState)
{
	// Generate the next output samples
	ITASampleFrame& sfOutput = *vOutputs[0].pBuf;
	float* pfOutputData = sfOutput[0].GetData();

	// Variables
	const double dSamplerate = GetStreamProperties().dSampleRate;
	const int iBlocklength = GetStreamProperties().iBlockLength;
	int N = m_iPeriodLengthSamples;
	float a = m_fAmplitude;
	
	float omega;
	float gradient = a / (float) (N-1);	// Steigung der S�gezahn und Dreieck
	int iZero;							// Offset �bergang 1=>0 bei Rechteck
	int iNumSamples;					// Anzahl zu erzeugender Samples
	
	switch (m_iFunction) {

	case SINE:

		omega = ITAConstants::TWO_PI_F / (float) N; // 2*pi/T
		
		iNumSamples = m_bPeriodic ? iBlocklength : (std::min)( iBlocklength, m_iPeriodLengthSamples - m_iSampleCount);

		for (int i=0;i<iNumSamples;i++) {
			pfOutputData[i] = a * sin(omega*i + m_fPhase);
		}

		m_fPhase = fmodf(iNumSamples*omega + m_fPhase, ITAConstants::TWO_PI_F);

		m_iSampleCount += iNumSamples;
		
		break;

	case TRIANGLE:

		iNumSamples = (m_bPeriodic ? iBlocklength : (std::min)( iBlocklength, m_iPeriodLengthSamples - m_iSampleCount) );

		for (int i=0;i<iNumSamples;i++) {
			float x =  fmodf((float) m_iSampleCount++, (float) m_iPeriodLengthSamples);

			pfOutputData[i] = x < N/2 ?  2*(x*gradient) : 2*((-x*gradient)+a);
		}

		break;
		
	case SAWTOOTH:  // S�gezahn

		iNumSamples = (m_bPeriodic ? iBlocklength : (std::min)( iBlocklength, m_iPeriodLengthSamples - m_iSampleCount) );
		
		for (int i=0;i<iNumSamples;i++) {

			float x = fmodf((float) m_iSampleCount++, (float) m_iPeriodLengthSamples);
			pfOutputData[i] = a * (x*gradient);
		}

		break;
			
	case RECTANGLE:

		// Position des Wechsels von 1 zu 0 innerhalb einer Periode
		iZero = (int)(m_iPeriodLengthSamples / 2);
		iNumSamples = (m_bPeriodic ? iBlocklength : (std::min)( iBlocklength, m_iPeriodLengthSamples - m_iSampleCount) );

		for (int i=0; i<iNumSamples; i++) 
		{
			// Position innerhalb einer Periodenl�nge
			float x = fmodf((float) m_iSampleCount++, (float) m_iPeriodLengthSamples);
			int iOffset = (int) roundf(x);
			pfOutputData[i] = (iOffset < iZero ? a : 0);
		}

		break;

	case DIRAC:
		
		iNumSamples = (m_bPeriodic ? iBlocklength : (std::min)( iBlocklength, m_iPeriodLengthSamples - m_iSampleCount) );

		if (m_bPeriodic) {
			for (int i=0;i<iNumSamples;i++) pfOutputData[i] = ( m_iSampleCount++ % N ? 0 : a );
		} else {
			pfOutputData[0] = (m_iSampleCount == 0 ? a : 0);
			for (int i=1;i<iNumSamples;i++) pfOutputData[i] = 0;
			m_iSampleCount += iNumSamples;
		}

		break;

	case WHITE_NOISE: 

		srand(100);
		for(int i= 0; i<iBlocklength; i++)
			pfOutputData[i] = a * (float) rand()/ RAND_MAX;
		
		break;	
	// ...

	default:
		// Case: Invalid signal function => Generate zeros
		sfOutput.zero();
		return;
	}

	/*
	 *  Since all output channels share the same data,
	 *  we calculate it just for the first channel and
	 *  then copy the data into all further channels.
	 */

	for (int c=1; c<sfOutput.channels(); c++)
		sfOutput[c] = sfOutput[0];
}
