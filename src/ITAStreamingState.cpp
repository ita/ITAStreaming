#include <ITAStreamingState.h>

#include <limits>

const long ITAStreamingState::UNDEFINED = std::numeric_limits< long >::max();

ITAStreamingState::ITAStreamingState()
: nSamples( 0 )
, dTimeCode( 0 )
{

}

ITAStreamingState::~ITAStreamingState()
{

};

bool ITAStreamingState::operator==( const ITAStreamingState& rhs ) const
{
	return !(*this != rhs);
}

bool ITAStreamingState::operator!=( const ITAStreamingState& rhs ) const
{
	return ( ( nSamples != rhs.nSamples ) || ( dTimeCode != rhs.dTimeCode ) );
}
