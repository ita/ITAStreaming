/*
   +---------------------------------------------------------+
   |                                                         |
   |  ITADatasource.h                                        |
   |  Abstrakte Basisklasse f�r Datenquellen                 |
   |                                                         |
   |  Autoren:  Frank Wefers, Tobias Lentz                   |
   |                                                         |
   |  (c) Copyright Institut f�r technische Akustik (ITA)    |
   |      Aachen university of technology (RWTH), 2005       |
   |                                                         |
   +---------------------------------------------------------+
                                                               */
// $Id: ITADatasource.h,v 1.7 2008-12-12 12:52:43 fwefers Exp $


#include "ITAStreamingSource.h"

#include <ITAException.h>
