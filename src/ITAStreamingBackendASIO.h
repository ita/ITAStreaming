/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAMING_BACKEND_ASIO
#define INCLUDE_WATCHER_ITA_STREAMING_BACKEND_ASIO

// ITAStreaming
#include <ITAStreamingDefinitions.h>
#include <ITAStreamingBackend.h>

//! Audio backend implemented using Steinberg's ASIO SDK (singleton)
class ITA_STREAMING_API ITAStreamingBackendASIO : public ITAStreamingBackend
{
public:
	static ITAStreamingBackendASIO* GetInstance();
};

#endif // INCLUDE_WATCHER_ITA_STREAMING_BACKEND_ASIO
