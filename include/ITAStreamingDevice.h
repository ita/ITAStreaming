/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAM_DEVICE
#define INCLUDE_WATCHER_ITA_STREAM_DEVICE

// ITAStreaming
#include <ITAStreamingDefinitions.h>
#include <ITAStreamingProperties.h>

// STL
#include <string>


//! Audio backend interface
class ITA_STREAMING_API ITAStreamingDeviceProperties
{
public:
	std::string sName;
	int iBlockSize;
	double dSampleRate;
	int iNumInputChannels;
	int iNumOutputChannels;

	ITAStreamingDeviceProperties( std::string sName, double dSampleRate, int iBlockSize, int iNumInputChannels, int iNumOutputChannels )
		: sName(sName), dSampleRate(dSampleRate), iBlockSize(iBlockSize), iNumInputChannels(iNumInputChannels), iNumOutputChannels( iNumOutputChannels )
	{
	}

	bool IsValid() const;
};

class ITAStreamDevice
{
public:
	ITAStreamDevice(const ITAStreamingProperties&);
};

#endif // INCLUDE_WATCHER_ITA_STREAM_DEVICE
