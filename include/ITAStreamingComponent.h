/*
* ----------------------------------------------------------------
*
*		ITA core libs
*		(c) Copyright Institute of Technical Acoustics (ITA)
*		RWTH Aachen University, Germany, 2015-2021
*
* ----------------------------------------------------------------
*				    ____  __________  _______
*				   //  / //__   ___/ //  _   |
*				  //  /    //  /    //  /_|  |
*				 //  /    //  /    //  ___   |
*				//__/    //__/    //__/   |__|
*
* ----------------------------------------------------------------
*
*/

#ifndef INCLUDE_WATCHER_ITA_STREAM_COMPONENT
#define INCLUDE_WATCHER_ITA_STREAM_COMPONENT

// ITA Base
#include <ITASampleFrame.h>

// ITA Streaming
#include <ITAStreamingDefinitions.h>
#include <ITAStreamingProperties.h>
#include <ITAStreamingState.h>

// STL
#include <unordered_map>

class ITAStreamingComponent;
class ITAStreamingComponentInput;
class ITAStreamingComponentOutput;
class ITAStreamingComponentInputImpl;
class ITAStreamingComponentOutputImpl;

// Schnittstelle: Nur f�r logische Steuerung nicht f�r interne Verdrahtung.
class ITA_STREAMING_API ITAStreamingComponentInput {
public:
	virtual ~ITAStreamingComponentInput() {};

	////! Returns the ID
	//virtual int GetID() const=0;

	////! Returns the parent component
	//virtual ITAStreamComponent& GetStreamComponent() const=0;

	//! Returns the properties of the audio stream
	virtual const ITAStreamingProperties& GetStreamProperties() const=0;

	//! Return if the input is connected to an output
	virtual bool IsConnected() const=0;

	//! Connect the input to an output
	// Behavior: Error if already connected.
	virtual void Connect(ITAStreamingComponentOutput&)=0;

	//! Disconnect the input
	// Behavior: No error if not connected.
	virtual void Disconnect()=0;
};

// Schnittstelle: Nur f�r logische Steuerung nicht f�r interne Verdrahtung.
class ITA_STREAMING_API ITAStreamingComponentOutput
{
public:
	virtual ~ITAStreamingComponentOutput() {};

	////! Returns the ID
	//virtual int GetID() const=0;

	////! Returns the parent component
	//virtual ITAStreamComponent& GetStreamComponent() const=0;

	//! Returns the properties of the audio stream
	virtual const ITAStreamingProperties& GetStreamProperties() const=0;

	//! Connect the input to an output
	// TODO: Ausgang darf nur einmalig beschaltet sein!
	virtual void Connect(ITAStreamingComponentInput&)=0;
};

//// Interface: Stream component
//class ITAStreamComponent {
//public:
//	virtual ~ITAStreamComponent() {};
//
//	// --= Inputs =--
//
//	//! Return the numbers of inputs
//	int GetNumInputs() const;
//
//	//! Access an input
//	// Note: Throws an ITAException the input does not exist
//	ITAStreamComponentInput& GetInput(int iID) const;
//
//	// --= Outputs =--
//
//	//! Return the numbers of outputs
//	int GetNumOutputs() const;
//
//	//! Access an output
//	// Note: Throws an ITAException the output does not exist
//	ITAStreamComponentOutput& GetOutput(int iID) const;
//};

//class ITAStreamComponentInputMap {
//public:
//	typedef std::unordered_map<int, ITAStreamComponentInput*> Map;
//	typedef Map::iterator Iterator;
//	typedef Map::const_iterator ConstIterator;
//
//	//! Returns the number of inputs
//	int GetNumInputs() const;
//
//	//! Access operators (by ID)
//	// Note: Throws an ITAException if the input does not exist
//	const ITAStreamComponentInput& operator[](int iID) const;
//
//	ConstIterator& Begin() const;
//	ConstIterator& End() const;
//
//	//ITAStreamComponentInput operator[](int iID);
//};

/*
  Gesamte komponente arbeitet mir gleichen Streamprops
*/
//! 
class ITA_STREAMING_API ITAStreamingComponent {
public:
	//! Behaviour variants
	typedef enum {
		PROCESS_COMPONENT=0, //!< Processing component (outputs are generated from inputs. ProcessStream is called.)
		TRIGGER_COMPONENT,   //!< Trigger component (no coupling between inputs an outputs. ProcessStream is not called. Manual triggering using TriggerStream).
	} ITAStreamComponentMode;

	typedef std::unordered_map<int, ITAStreamingComponentInput*> InputMap;
	typedef std::unordered_map<int, ITAStreamingComponentOutput*> OutputMap;

	//! Creates an empty components without inputs and outputs
	explicit ITAStreamingComponent(const ITAStreamingProperties& oProps, ITAStreamComponentMode eMode=PROCESS_COMPONENT);

	//! Returns the properties of the audio stream
	// Caution: Channels is always 1 here... Refactor and improve arch...
	const ITAStreamingProperties& GetStreamProperties() const;

	void Reset();

	// --= Inputs =--

	//! Return the numbers of inputs
	int GetNumInputs() const;

	//! Access an input
	// Note: Throws an ITAException the input does not exist
	ITAStreamingComponentInput& GetInput(int iID) const;

	//! Returns all inputs
	//std::map<ITAStreamComponentInput&> GetInputs()

	//! Adds an input and returns its ID
	int AddInput(int nChannels);
	
	//! Removes an input and returns its ID
	void RemoveInput(int iID);

	// --= Outputs =--

	//! Return the numbers of outputs
	int GetNumOutputs() const;

	//! Access an output
	// Note: Throws an ITAException the output does not exist
	ITAStreamingComponentOutput& GetOutput(int iID) const;

	//! Adds an output and returns its ID
	int AddOutput(int nChannels);

	//! Removes an output and returns its ID
	void RemoveOutput(int iID);

	//! --= Connections =--

	//! Connect an input to the output of another component
	//void ConnectInput(int iInputID, ITAStreamComponent* pSrc, int iSrcOutputID);

	//! Disconnects an input
	//void DisconnectInput(int iInputID);

	//bool IsInputConnected()
	//bool IsOutputConnected()

protected:
	class InputDesc {
	public:
		int iID;
		const ITASampleFrame* pBuf;
		// M�gliche Zusatzfelder: bool bConnected
	};

	class OutputDesc {
	public:
		int iID;
		ITASampleFrame* pBuf;
		// M�gliche Zusatzfelder: bool bConnected
	};

	typedef std::vector<InputDesc> InputList;
	typedef std::vector<OutputDesc> OutputList;

	// Konstruktionsarbeit!
	// TODO: Zugriffsmethoden raus und durch Struktur ersetzen.

	////! Returns the buffer of an input
	const ITASampleFrame& GetStreamInputBuffer(int iInputID);

	////! Returns the buffer of an output
	ITASampleFrame& GetStreamOutputBuffer(int iOutputID);

	// Diese Paar von Funktionen besser erl�utern

	// This internal method requests all depending components
	// on the input side to produce their next output samples
	void PullInputs(const ITAStreamingState& oStreamState);

	// This method is called by depending components to request
	// the production of output samples on the component
	void PullOutputs(const ITAStreamingState& oStreamState);

	//! Hook: Process the audio samples of the stream
	// Niemals selber aufrufen.
	virtual void ProcessStream(const ITAStreamingComponent::InputList& vInputs,
		                       const ITAStreamingComponent::OutputList& vOutputs,
							   const ITAStreamingState& oStreamState);

private:
	const ITAStreamingProperties m_oStreamProps;
	ITAStreamComponentMode m_eMode;
	ITAStreamingState m_oStreamState;	// Last stream state
	ITASampleFrame m_sfSilence;	// One block of zeros - used for unconnected inputs
	
	InputMap m_pmInputs;
	int m_iInputIDCounter;

	OutputMap m_pmOutputs;
	int m_iOutputIDCounter;
	
	InputList m_vInputList;
	OutputList m_vOutputList;

	// Table: Pending components on the input-side (unique!)
	typedef std::vector< std::pair<ITAStreamingComponent*, int> > ComponentTable; // (ptr, numrefs)
	ComponentTable m_tInputComponents;

	//! --= Connections =--

	//! Connect an input to the output of another component
	//void ConnectInput(int iInputID, ITAStreamComponent* pSrc, int iSrcOutputID);
	void ConnectInput(ITAStreamingComponentInputImpl* pInput, ITAStreamingComponentOutputImpl* pOutput);

	//! Disconnects an input
	//void DisconnectInput(int iInputID);
	void DisconnectInput(ITAStreamingComponentInputImpl* pInput);

	friend class ITAStreamingComponentInputImpl;
};

//! Connect the output of component to the input of another component
// Note: This easy-to-use function only works if the source component has
//       exactly one output and the destination component has exactly one input.
//       If the components have multiple inputs or outputs you need to use
//       the refined connect function (see below)
void ITA_STREAMING_API Connect(ITAStreamingComponent* pSource, ITAStreamingComponent* pDest);

//COMPONENT c;
//connect(c.inputs[0], d.outputs[1]);
//
////! Interface: Component with one input and one output
//class ITAStreamComponent11 {
//public:
//
//};


#endif // INCLUDE_WATCHER_ITA_STREAM_COMPONENT
