/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAMDATASOURCE
#define INCLUDE_WATCHER_ITA_STREAMDATASOURCE

#include <ITAStreamingDefinitions.h>
#include <ITAStreamingProperties.h>
#include <ITAStreamingState.h>

#include <ITASampleFrame.h>

//#include <ITAAtomicPrimitives.h>

class ITA_STREAMING_API ITAStreamingSource
{
public:
    //! Destruktor
	virtual ~ITAStreamingSource() {}

	//! Returns the properties of the audio stream
	virtual const ITAStreamingProperties& GetStreamProperties() const=0;

	//! Returns the data (samples) of a specific channel of the audio stream
	// DOKU:
	//! Blockzeiger abrufen
	/**
	 * Diese Methode gibt den Zeiger auf das float-Array zur�ck,
	 * in dem der n�chsten Block Daten gespeichert ist. Solch ein
	 * Zeiger wird auch Blockzeiger genannt.
	 *
	 * \param uiChannel Index des Kanals (Wertebereich: [0, Kan�le-1])
	 * \param pStreamInfo Zustandsinformationen des Streams (niemals nullptr)
	 *
	 * \return Zeiger auf das float-Array mit den n�chsten Daten
	 *         des angegebenen Kanals
	 *
	 * \note Falls momentan keine Daten verf�gbar sind,
	 *       wird der <b>Nullzeiger</b> zur�ckgegeben.
	 *
	 * \note <b>Testen</b> Sie die <b>Verf�gbarkeit von Daten</b> durch den Aufruf 
	 *       der Methode mit einem beliebigen g�ltigen Kanal und bWaitForData==false.
	 *       Erhalten Sie in diesem Falle den Nullzeiger zur�ck, sind keine 
	 *       Daten verf�gbar.
	 *
	 * \warning Der Aufruf der Methode mit einem ung�ltigen Index (siehe Wertebereich)
	 *          f�hrt <b>auch</b> zur R�ckgabe eines <b>Nullzeigers</b>.
	 */

	// Important: Nicht reentrant. wird nicht gepr�ft. Falls passiert BUMM!!
	// Darf niemals Null zur�ckgeben!!!
	virtual const ITASampleFrame& GetStreamData(const ITAStreamingState& oState)=0;	
	
	// TODO: Braucht man das ganze noch?

	////! Anzahl der Benutzungen der Datenquelle zur�ckgeben
	//virtual int GetNumReferences();

	////! Entfernt alle Referenzen (setzt Referenzz�hler auf 0)
	//virtual void ClearReferences();

	////! Benutzung vermerken
	///**
	// * Erlaubt Benutzern der Datenquelle ihre Benutzung zu vermerken.
	// * Die Implementierung erfolg mittels Referenzz�hlern. Auf diese
	// * einfache Weise kann sichergestellt werden, das eine Datenquelle
	// * nicht gel�scht wird, sofern sie noch benutzt wird.
	// *
	// * \return Anzahl der Referenzen nach dem Hinzuf�gen
	// */
	//virtual int AddReference();

	////! Benutzung vermerken
	///**
	// * Erlaubt Benutzern der Datenquelle ihre Benutzung zu vermerken.
	// * Die Implementierung erfolg mittels Referenzz�hlern. Auf diese
	// * einfache Weise kann sichergestellt werden, das eine Datenquelle
	// * nicht gel�scht wird, sofern sie noch benutzt wird.
	// *
	// * \return Anzahl der Referenzen nach dem Entfernen
	// */
	//virtual int RemoveReference();

protected:
	//! Gesch�tzter Standardkonstruktor
	ITAStreamingSource() {};

private:
	//ITAAtomicInt m_iRefCount;
};

#endif // INCLUDE_WATCHER_ITA_STREAMDATASOURCE
