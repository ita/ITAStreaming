/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAM_STATE
#define INCLUDE_WATCHER_ITA_STREAM_STATE

#include <ITAStreamingDefinitions.h>

//! This class describes the state of a streaming block (frame)
class ITA_STREAMING_API ITAStreamingState
{
public:
	static const long int UNDEFINED; //! Used for comparison

	long int nSamples;	//!< Number of samples played back until start of streaming
	double dTimeCode;	//!< Elapes time in seconds until start of streaming

	ITAStreamingState();
	virtual ~ITAStreamingState();

	//! Comparator eqal (bit-wise)
	bool operator==( const ITAStreamingState& ) const;

	//! Comparator uneqal (bit-wise)
	bool operator!=( const ITAStreamingState& ) const;
};

#endif // INCLUDE_WATCHER_ITA_STREAM_STATE
