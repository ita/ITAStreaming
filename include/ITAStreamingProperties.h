/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAM_PROPERTIES
#define INCLUDE_WATCHER_ITA_STREAM_PROPERTIES

#include <ITAStreamingDefinitions.h>

//! Properties of an audio stream
class ITA_STREAMING_API ITAStreamingProperties
{
public:
	double dSampleRate;		//!< Audio sampling rate [Hertz]
	int iNumChannels;		//!< Number of channels
	int iBlockLength;		//!< Audio streaming block length (buffer size, frame size) [Samples]

	ITAStreamingProperties();

	explicit ITAStreamingProperties( double dSampleRate, int iNumChannels, int iBlockLength );

	virtual ~ITAStreamingProperties();

	//! Comparison operator
	/**
	  * A comparison results in a true statement, if the properties sample rate, channel number and block length are equal.
	  */
	bool operator==( const ITAStreamingProperties& rhs ) const;
	bool operator!=( const ITAStreamingProperties& rhs ) const;

	//! Validity check
	/**
	  * A valid stream has a sample rate and block size greater zero and at least a single channel.
	  */
	bool IsValid() const;
};

#endif // INCLUDE_WATCHER_ITA_STREAM_PROPERTIES
