/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAM_BACKEND
#define INCLUDE_WATCHER_ITA_STREAM_BACKEND

// ITAStreaming
#include <ITAStreamingDefinitions.h>
#include <ITAStreamingDevice.h>

// STL
#include <vector>

//! Audio backend interface
class ITA_STREAMING_API ITAStreamingBackend {
public:
	static ITAStreamingBackend* GetDefaultBackend();

	virtual ~ITAStreamingBackend() {};

	//! Enumerates the available ASIO audio devices
	// Note: Only the field sName will be set in each entry
	virtual std::vector<ITAStreamingDeviceProperties> EnumerateDevices()=0;

	//! Initializes an ASIO audio device for streaming
	virtual ITAStreamDevice* InitializeDevice( const ITAStreamingDeviceProperties& oProps )=0;
};

#endif // INCLUDE_WATCHER_ITA_STREAM_BACKEND
