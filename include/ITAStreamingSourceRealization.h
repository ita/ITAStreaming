/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STREAM_SOURCE_REALIZATION
#define INCLUDE_WATCHER_ITA_STREAM_SOURCE_REALIZATION

#include <ITAStreamingDefinitions.h>
#include <ITAStreamingSource.h>

// Vorw�rtsdeklarationen
class ITAStreamDatasourceRealizationEventHandler;

//! Gr�ndger�st f�r die Implementierung von Datenquellen
/**
 * Die Klasse ITADatasourceRealization ist ein Grundger�st f�r die Implementierung
 * von Datenquellen. Sie bietet dem Entwickler einen Einstiegspunkt eigene
 * Datenquellen zu konstruieren und fa�t einige grundlegende Funktionalit�t
 * zusammen. ITADatasourceRealization realisiert die abstrakten Methoden
 * der Oberklasse ITADatasource und bietet dar�ber hinaus konkrete Methoden
 * f�r das Bereitstellen von Daten seitens der Datenquelle.
 *
 * Intern enth�lt ein ITADatasourceRealization einen Puffer in dem die 
 * freizusetzenden Daten zwischengespeichert werden. Dieser Puffer
 * kann nach beliebig dimensioniert werden (f�r die Thread-Sicherheit mu� er
 * aber mindestens die Gr��e 2 haben). Die Klasse stellt Methoden bereit
 * um Zeiger auf diesen Puffer abzurufen und diese zu inkrementieren (analog
 * der publizierten Schnittstelle von ITADatasource). Dabei werden komplexe
 * Aspekte der Thread-Sicherheit ber�cksichtigt und m�ssen nicht vom Entwickler
 * selbst programmiert werden.
 *
 * \ingroup datasources
 */


// Module m�ssen immer Ausgabedaten schreiben
class ITA_STREAMING_API ITAStreamingSourceRealization : public ITAStreamingSource
{
public:
	ITAStreamingSourceRealization(const ITAStreamingProperties& oProperties);
	virtual ~ITAStreamingSourceRealization();

	//! Reset
	/**
	 * Clear internal state, reset stream.
	 * \important May only be called, when the streaming is not running!
	 */
	virtual void Reset();

	//! Handler f�r Stream-Event setzen
	/**
	 * \important Darf nur aufgerufen werden, wenn das Streaming nicht l�uft!
	 */
	ITAStreamDatasourceRealizationEventHandler* GetStreamEventHandler() const;

	//! Handler f�r Stream-Event setzen
	/**
	 * \important Darf nur aufgerufen werden, wenn das Streaming nicht l�uft!
	 */
	void SetStreamEventHandler(ITAStreamDatasourceRealizationEventHandler* pHandler);

	//! Returns the output buffer of a specific channel (used for writing)
	ITASampleFrame& GetStreamOutputBuffer();

	// --= Interace "ITAStreamDatasource" =--

	const ITAStreamingProperties& GetStreamProperties() const { return m_oStreamProps; }
    virtual const ITASampleFrame& GetStreamData(const ITAStreamingState& oState);
	
protected:
	const ITAStreamingProperties m_oStreamProps;

	//! Hook: Process the audio samples of the stream
	/**
	 * Diese Hook-Methode wird von der Klasse aufgerufen, wenn neue Stream-Daten
	 * produziert werden sollen. Unterklassen sollten diese Methode redefinieren, 
	 * um die Verarbeitung von Samples zu realisieren.
	 */
	virtual void ProcessStreamData(const ITAStreamingState& oStreamState) {};
	
private:
	ITAStreamDatasourceRealizationEventHandler* m_pEventHandler;
	ITASampleFrame m_oOutputBuffer;
	long m_iStreamPos;		// Last position of the stream (needed for update checks)
	
	friend class ITAStreamDatasourceRealizationEventHandler;
};

//! Schnittstelle f�r Nachrichten-Verarbeitung der Klasse ITADatasourceRealization
class ITA_STREAMING_API ITAStreamDatasourceRealizationEventHandler
{
public:
	virtual ~ITAStreamDatasourceRealizationEventHandler() {};
	virtual void HandleProcessStreamData(ITAStreamingSourceRealization* pSender, const ITAStreamingState& oStreamState);
};

#endif // INCLUDE_WATCHER_ITA_STREAM_SOURCE_REALIZATION
