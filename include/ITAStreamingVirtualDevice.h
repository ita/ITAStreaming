// $Id: $

#ifndef INCLUDE_WATCHER_ITA_STREAMVIRTUALDEVICE
#define INCLUDE_WATCHER_ITA_STREAMVIRTUALDEVICE

#include <ITAStreamingDefinitions.h>
#include <ITAStreamingComponent.h>

//! Virtual audio device helpful for debugging
class ITA_STREAMING_API ITAStreamingVirtualDevice : public ITAStreamingComponent
{
public:
	// Input channels = Record channels on the virtual device
	// Output channels = Playback channels on the virtual device
	ITAStreamingVirtualDevice(double dSamplerate, int iInputChannels, int iOutputChannels, int iBlocklength);
	virtual ~ITAStreamingVirtualDevice();

	void Reset();
	void Trigger();

private:
	ITAStreamingState m_oStreamState;
};

#endif // INCLUDE_WATCHER_ITA_STREAMVIRTUALDEVICE
